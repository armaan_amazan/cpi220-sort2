package org.gjt.sp.jedit.cpi220.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.gjt.sp.jedit.cpi220.performance.Stopwatch;


public class MainSort {

	public static void sortRandom(Surname[] surnames)
	{
		Surname[] tempS = surnames;
		while (tempS.length >= 1)
		{
			tempS = Arrays.copyOfRange(tempS, 0, tempS.length/2);
			Shuffle.shuffle(tempS);

			// Add stopwatch here
			Stopwatch sw = new Stopwatch();
			
			// Sort
			System.out.println("Starting sorting...");
			Heap.sort(tempS);
			
			// Stop stopwatch
			double time = sw.elapsedTime(); // Stop watch here
			System.out.println("Elapsed time:" + time);
			IO.write(tempS, "output" + tempS.length + ".csv");
			
		}
	}
	
	public static void sortSorted(Surname[] surnames)
	{		
		Surname[] tempS = surnames;
		//Insertion.sort(tempS);
		while (tempS.length >= 1)
		{
			//Shuffle.shuffle(tempS);

			// Add stopwatch here
			Stopwatch sw = new Stopwatch();
			
			// Sort
			System.out.println("Starting sorting...");
			Insertion.sort(tempS);
			
			// Stop stopwatch
			double time = sw.elapsedTime(); // Stop watch here
			System.out.println("Elapsed time:" + time);
			IO.write(tempS, "output.csv");
			tempS = Arrays.copyOfRange(tempS, 0, tempS.length/2);

		}
	}
	
	public static void sortNearlySorted(Surname[] surnames)
	{
		Surname[] tempS = surnames;
		tempS = Arrays.copyOfRange(tempS, 0, tempS.length/10);
		int size = 10000;
		Insertion.sort(tempS);
		for (int i = 0; i < tempS.length; i+=size)
		{
			Shuffle.shuffle(tempS, 0, i);

			// Add stopwatch here
			Stopwatch sw = new Stopwatch();
			
			// Sort
			System.out.println("Starting sorting...");
			Insertion.sort(tempS);
			
			// Stop stopwatch
			double time = sw.elapsedTime(); // Stop watch here
			System.out.println("Elapsed time:" + time);
			IO.write(tempS, "output.csv");

		}
	}
	
	public static void main(String[] args) {
		
		// Read CSV
		Surname[] surnames = IO.read("surnames.csv");
		MainSort.sortRandom(surnames);
		
		// Write to CSV
		
	}
	
}
